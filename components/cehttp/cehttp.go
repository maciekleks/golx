package cehttp

import (
	"context"
	"fmt"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"

	//log "github.com/sirupsen/logrus"
	//cloudevents "github.com/cloudevents/sdk-go/v2"
	"net/http"
	"strconv"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	cectx "github.com/cloudevents/sdk-go/v2/context"
	c "gitlab.com/maciekleks/golx/core"
)

type ISourceCloudEventsHttp interface {
	c.ISource
	handle(c.CoreContext, c.FlowFunc, *c.Params) http.HandlerFunc
}
type SourceCloudEventsHttp struct {
}

type CloudEventsHttpError struct {
	error
}

// type aliasing to use only golx types
type CloudEventsRetryParams = cectx.RetryParams

// constants aliasing to use only golx one
const (
	BackoffStrategyNone        = cectx.BackoffStrategyNone
	BackoffStrategyConstant    = cectx.BackoffStrategyConstant
	BackoffStrategyLinear      = cectx.BackoffStrategyLinear
	BackoffStrategyExponential = cectx.BackoffStrategyExponential
	//Params Map index of url option
	GolxCEHttpURL = "url"
	//Params Map index of retryParams option
	GolxCEHttpRetryParams = "retryParams"
)

// responseWriter is a minimal wrapper for http.ResponseWriter that allows the
// written HTTP status code to be captured for logging.
// src: https://blog.questionable.services/article/guide-logging-middleware-go/
type responseWriter struct {
	http.ResponseWriter
	status      int
	wroteHeader bool
}

func WithUrl(url string) c.GateOption {
	return func(params *c.Params) {
		(*params)[GolxCEHttpURL] = url
	}
}

func WithRetryParams(retryParams *CloudEventsRetryParams) c.GateOption {
	return func(params *c.Params) {
		(*params)[GolxCEHttpRetryParams] = retryParams
	}
}

func wrapResponseWriter(w http.ResponseWriter) *responseWriter {
	return &responseWriter{ResponseWriter: w}
}

func (rw *responseWriter) Status() int {
	return rw.status
}

func (rw *responseWriter) WriteHeader(code int) {
	if rw.wroteHeader {
		return
	}

	rw.status = code
	rw.ResponseWriter.WriteHeader(code)
	rw.wroteHeader = true

	return
}

// LoggingMiddleware logs the incoming HTTP request & its duration.
func loggingMiddleware(logger *zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					logger.Fatal().
						Stack().
						Msgf("Internal Server Error: %v", err)
				}
			}()

			start := time.Now()
			wrapped := wrapResponseWriter(w)
			next.ServeHTTP(wrapped, r)
			logger.Info().
				Int("status", wrapped.status).
				Str("method", r.Method).
				Str("path", r.URL.EscapedPath()).
				Dur("duration[ms]", time.Duration(time.Since(start).Milliseconds())).
				Msg("")
		})
	}
}

func getHeaders(r *http.Request, pexchange *c.Exchange) {
	for key, value := range r.Header {
		pexchange.Header[key] = value
	}
}

func (self *SourceCloudEventsHttp) Register(cctx c.CoreContext, pattern *string, endpoint *c.Endpoint, mux *mux.Router) {

	p, err := cloudevents.NewHTTP()
	if err != nil {
		cctx.Config.Logger.Fatal().Msgf("Failed to create CloudEvents protocol: %s", err.Error())
	}
	h, err := cloudevents.NewHTTPReceiveHandler(cctx, p, self.handle(cctx, endpoint.To, endpoint.Params))
	if err != nil {
		cctx.Config.Logger.Fatal().Msgf("Failed to create CloudEvents handler: %s", err.Error())
	}
	mux.Handle(*pattern, h)
}

func (self *SourceCloudEventsHttp) handle(cctx c.CoreContext, f c.FlowFunc, p *c.Params) func(context.Context, cloudevents.Event) {
	return func(ctx context.Context, event cloudevents.Event) {

		fmt.Printf("handle event: %+v\n\n\n", event)

		pexchange := &c.Exchange{
			map[string]interface{}{c.ID: strconv.Itoa(1)},
			"cloudevent",
			event,
			p,
			nil,
		}

		//res := c.OrDone(cctx, f(cctx, c.Raw(cctx, pexchange))) //TODO: Handle the result
		//out, _ := <-res.Stream //the stream is closed if ok==false
		//cctx.Config.Logger.Debug().Msgf("CE flow out: %+v", out)

		c.Sink(cctx, c.OrDone(cctx, f(cctx, c.Raw(cctx, pexchange)))) //TODO: Handle the result
	}
}

var (
	egressRequestDuration = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "golx_egress_request_duration_seconds",
		Help:    "Histogram for the runtime of a simple example function.",
		Buckets: prometheus.LinearBuckets(0.01, 0.01, 10),
	})
)

func init() {
	// Metrics have to be registered to be exposed:
	prometheus.MustRegister(egressRequestDuration)
}

// Sends only one event at a time
// TODO: sends many events from slice
// TODO: p and client should be cached
func CloudEventsHttp(cctx c.CoreContext, pin *c.Gate, fs ...func(cctx c.CoreContext, pexchange *c.Exchange) error) *c.Gate {

	fmt.Println("-------------------------- 2 PARAMS:", pin.Params)
	params := pin.Params //TODO: can be nil if Http is run by To instead of ToP
	//return pin.To(core.Log)
	return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error {
		fmt.Println("-------------- in 2 TO")
		fmt.Println("-------------- in TO2")
		// https://prometheus.io/docs/practices/histograms/ for differences.
		timer := prometheus.NewTimer(egressRequestDuration)

		fmt.Println("-------------- in TO3")
		defer timer.ObserveDuration()

		fmt.Printf("////CloudEventsHTTP START for params: %+v\n\n\n", params)

		url, ok := (*params)[GolxCEHttpURL].(string)
		if !ok {
			return CloudEventsHttpError{c.WrapError(nil, "%s parameter not set", GolxCEHttpURL)}
		}
		fmt.Printf("////HTTP PARAMS: URL: %v\n\n\n", url)
		ctx := cloudevents.ContextWithTarget(cctx, url)

		retryParams, ok := (*params)[GolxCEHttpRetryParams].(*cectx.RetryParams)
		if ok {
			fmt.Printf("_____0 RETRY PARAMS SET")
			ctx = cectx.WithRetryParams(ctx, retryParams)
		} else {
			fmt.Printf("_____0 RETRY PARAMS NOT SET")
		}

		fmt.Printf("_____1 - new HTTP")
		p, err := cloudevents.NewHTTP()
		if err != nil {
			cctx.Config.Logger.Fatal().Msgf("Failed to create CloudEvents protocol: %s", err.Error())
		}

		fmt.Printf("_____2 - new client")
		client, err := cloudevents.NewClient(p, cloudevents.WithTimeNow(), cloudevents.WithUUIDs())
		if err != nil {
			cctx.Config.Logger.Fatal().Msgf("Failed to create CloudEvents client: %s", err.Error())
		}

		fmt.Printf("_____3 %+v\n\n\n", client)
		event, ok := pe.Body.(cloudevents.Event)
		if !ok {
			return CloudEventsHttpError{c.WrapError(nil, "Failed to parse Event from Body")}
		}

		fmt.Printf("_____4")
		if result := client.Send(ctx, event); cloudevents.IsUndelivered(result) {
			fmt.Printf("_____5.a")
			cctx.Config.Logger.Fatal().Msgf("Failed to send: %s", result.Error())
		} else if cloudevents.IsACK(result) {
			fmt.Printf("_____5.b")
			cctx.Config.Logger.Info().Msgf("Sent: %v %+v", result, event)
		} else if cloudevents.IsNACK(result) {
			fmt.Printf("_____5.c")
			//TODO Fatal exists with 1 and program craches
			cctx.Config.Logger.Fatal().Msgf("Sent but not accepted: %s", result.Error())
		}

		fmt.Printf("_____6.Done.")
		return nil
	},
	)
}
