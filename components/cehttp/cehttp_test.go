package cehttp

import (
	"bytes"
	"context"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	c "gitlab.com/maciekleks/golx/core"
)

func TestCloudEventsHttpSource(t *testing.T) {
	endpoint := c.Endpoint{
		From: &SourceCloudEventsHttp{},
		To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
			//return pin.To(cctx, c.Log)
			return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error {
				//cctx.Config.Logger.Debug().Msgf("UUUUUUUUUUUUUUU %v+", pe)
				fmt.Println("Exchange Event:", pe)
				return nil
			})
		},
	}

	var jsonBody = []byte(`{"data":"Test data."}`)
	req, err := http.NewRequest("POST", "/", bytes.NewBuffer(jsonBody))
	if err != nil {
		t.Fatal(err)
	}

	req.Header.Add("ce-source", `test`)
	req.Header.Add("ce-id", `1`)
	req.Header.Add("ce-specversion", `1.0`)
	req.Header.Add("ce-type", `golx-test-type`)
	req.Header.Add("content-type", `application/json`)

	resp := endpoint.MuxRouteTest(req)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: got: %d, expected: %d", resp.StatusCode, http.StatusOK)
	}
}

func TestCloudEventsHttp(t *testing.T) {
	const url = "127.0.0.1:9980"
	var pattern string = "/"
	// create a listener with the desired port.
	listener, err := net.Listen("tcp", url)
	if err != nil {
		t.Errorf("Can't bind to a port for the url: %s", url)
	}

	ch := make(chan *c.Exchange)
	endpoint := c.Endpoint{
		From: &SourceCloudEventsHttp{},
		To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
			//return pin.To(cctx, c.Log)
			return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error {
				//cctx.Config.Logger.Debug().Msgf("UUUUUUUUUUUUUUU %v+", pe)
				fmt.Println("Exchange Event:", pe)
				resch := (*pe.Params)["ch"].(chan *c.Exchange)

				resch <- pe
				close(resch)
				return nil
			}) /*.To(cctx, c.Sink)*/
		},
		Params: &c.Params{"ch": ch},
	}
	router := mux.NewRouter()
	logger := zerolog.New(os.Stdout).Level(zerolog.DebugLevel)
	cctx := c.New(context.TODO(), &c.ServeConfig{Logger: &logger}, "cctx")

	source := endpoint.From.(c.ISource)
	source.Register(cctx, &pattern, &endpoint, router)

	server := httptest.NewUnstartedServer(router)
	//set test listener not the default
	server.Listener.Close()
	server.Listener = listener

	server.Start()
	defer server.Close()

	event := cloudevents.NewEvent(cloudevents.VersionV1)
	//event.SetID("XYZ")
	event.SetType("golx-test-event")
	event.SetSource("golx-test")
	event.SetExtension("the", 42)
	event.SetExtension("heart", "yes")
	event.SetExtension("beats", true)

	fmt.Printf("\nevent after creation: %+v\n\n", event)

	in := &c.Exchange{
		map[string]interface{}{c.ID: 1},
		"cloudevent",
		event,
		nil,
		nil,
	}

	//	res := c.Raw(cctx, in).ToP(cctx, CloudEventsHttp, &c.Params{"url": "http://" + url})
	//cctx2 := c.New(context.TODO(), &c.ServeConfig{Logger: &logger}, "cctx2")
	//c.Sink(cctx2, c.Raw(cctx2, in).ToP(cctx2, CloudEventsHttp, &c.Params{"url": "http://" + url}))
	/*c.Sink(cctx, c.Raw(cctx, in).ToP(cctx, CloudEventsHttp, &c.Params{"url": "http://" + url, "retryParams": &CloudEventsRetryParams{
		Strategy: BackoffStrategyLinear,
		Period:   10 * time.Millisecond,
		MaxTries: 10,
	}}))*/
	c.Sink(cctx, c.Raw(cctx, in).
		ToP(cctx, CloudEventsHttp,
			c.ApplyOptions(
				WithUrl("http://"+url),
				WithRetryParams(&CloudEventsRetryParams{
					Strategy: BackoffStrategyLinear,
					Period:   10 * time.Millisecond,
					MaxTries: 10,
				}))))

	//cctx2.Wait()

	outpe := <-ch
	fmt.Println("OUTPE:", outpe)
	//server.Close()
	cctx.Wait() //TODO ?

	eventBack, ok := outpe.Body.(cloudevents.Event)
	if !ok {
		t.Errorf("Unexpected Body type received. Expected CloudEvents Event")
	}

	if err := eventBack.Validate(); err != nil {
		t.Errorf("Unexpected validation error: %v", err)
	}
}
