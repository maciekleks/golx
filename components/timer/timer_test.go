package timer

import (
	"testing"
	"time"

	c "gitlab.com/maciekleks/golx/core"
)

func TestTimerSource(t *testing.T) {
	got := "to be set"
	expected := "OK"
	done := make(chan bool)
	endpoint := c.Endpoint{
		From: &SourceTimer{1 * time.Second},
		To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
			return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error {
				got = expected
				close(done)
				return nil
			})
		},
	}

	endpoint.NonMuxRouteTest()
	<-done

	if got != expected {
		t.Errorf("Unexpected result: got: %s, expected: %s", got, expected)
	}
}
