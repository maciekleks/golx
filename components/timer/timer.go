package timer

import (
	"github.com/gorilla/mux"
	//log "github.com/sirupsen/logrus"
	"strconv"
	"time"

	c "gitlab.com/maciekleks/golx/core"
)

type ISourceTimer interface {
	c.ISource
	handle(c.CoreContext, c.FlowFunc, *c.Params)
}
type SourceTimer struct {
	Duration time.Duration
}

func (self *SourceTimer) Register(cctx c.CoreContext, pattern *string, endpoint *c.Endpoint, mux *mux.Router) {
	self.handle(cctx, endpoint.To, endpoint.Params)
}

func (self *SourceTimer) handle(cctx c.CoreContext, f c.FlowFunc, p *c.Params) {
	go func() {
		//cctx := c.NewCoreContext(ctx)
		c.Sink(cctx, f(cctx, c.Timer(cctx, self.Duration, func(iterNo int64, tick time.Time) *c.Exchange {
			//log.Println("handle Timer:: New Exchange is creating...")
			return &c.Exchange{
				map[string]interface{}{c.ID: strconv.FormatInt(iterNo, 10)},
				"timer",
				tick,
				p,
				nil,
			}
		})))

		cctx.Wait()
	}()

}

//For OldTimer
// use only one steamming
// func (self *SourceTimer) handleOld(ctx context.Context, f c.FlowFunc) {
// 	c.Sink(ctx, f(ctx, c.OldTimer(ctx, self.Duration)))
// }

// // every time create a new streaming
// func (self *SourceTimer) handleOld2(ctx context.Context, f c.FlowFunc) {

// 	go func() {
// 		pulse := time.NewTicker(self.Duration)
// 		defer pulse.Stop()
// 		defer log.Info("handle Timer goroutine stopped")

// 		iter := 0
// 		for {
// 			select {
// 			case <-ctx.Done():
// 				fmt.Println("handle Timer:: Gets ctx.Done().")
// 				return
// 			case tick := <-pulse.C:
// 				log.Println("handle Timer:: Tick comes in.")
// 				fmt.Println("handle Timer:: stdout Timer tick.")
// 				iter++
// 				innerCtx, _ := context.WithCancel(ctx) //to cancel only a route, not the whole timer
// 				//gCtx := c.GateContext{innerCtx, innerCancel}
// 				pexchange := &c.Exchange{
// 					map[string]interface{}{c.ID: strconv.Itoa(iter)},
// 					"time",
// 					tick,
// 					nil,
// 				}

// 				c.Sink(innerCtx, f(innerCtx, c.Raw(innerCtx, pexchange)))
// 			}
// 		}

// 	}()

// }
