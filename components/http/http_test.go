package http

//src: mux as ServHttp https://www.digitalflapjack.com/blog/2018/4/10/better-testing-for-golang-http-handlers
//src: https://blog.questionable.services/article/testing-http-handlers-go/

import (
	//"github.com/rs/zerolog"
	//"os"
	//"bytes"
	c "gitlab.com/maciekleks/golx/core"
	"io/ioutil"
	"net/http"
	//"net/http/httptest"
	"testing"
)

func TestHttpSource(t *testing.T) {
	expected := "OK"

	endpoint := c.Endpoint{
		From: &SourceHttp{},
		To: func(cctx c.CoreContext, pin *c.Gate) *c.Gate {
			return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error { pe.Body = expected; return nil })
		},
	}

	req, err := http.NewRequest("GET", "/", http.NoBody)
	if err != nil {
		t.Fatal(err)
	}

	resp := endpoint.MuxRouteTest(req)

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: got: %d, expected: %d", resp.StatusCode, http.StatusOK)
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		t.Errorf("Unexpected response body read error: got: %v, expected: nil", err)
	}
	if string(body) != expected {
		t.Errorf("Unexpected response body content: got: %v, expected: %s", string(body), expected)
	}

}
