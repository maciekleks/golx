package http

import (
	"bytes"
	"fmt"
	"io"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"

	//log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"time"

	c "gitlab.com/maciekleks/golx/core"
)

type ISourceHttp interface {
	c.ISource
	handle(c.CoreContext, c.FlowFunc, *c.Params) http.HandlerFunc
}
type SourceHttp struct {
}

type HttpError struct {
	error
}

// responseWriter is a minimal wrapper for http.ResponseWriter that allows the
// written HTTP status code to be captured for logging.
// src: https://blog.questionable.services/article/guide-logging-middleware-go/
type responseWriter struct {
	http.ResponseWriter
	status      int
	wroteHeader bool
}

func wrapResponseWriter(w http.ResponseWriter) *responseWriter {
	return &responseWriter{ResponseWriter: w}
}

func (rw *responseWriter) Status() int {
	return rw.status
}

func (rw *responseWriter) WriteHeader(code int) {
	if rw.wroteHeader {
		return
	}

	rw.status = code
	rw.ResponseWriter.WriteHeader(code)
	rw.wroteHeader = true

	return
}

// LoggingMiddleware logs the incoming HTTP request & its duration.
func loggingMiddleware(logger *zerolog.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					logger.Fatal().
						Stack().
						Msgf("Internal Server Error: %v", err)
				}
			}()

			start := time.Now()
			wrapped := wrapResponseWriter(w)
			next.ServeHTTP(wrapped, r)
			logger.Info().
				Int("status", wrapped.status).
				Str("method", r.Method).
				Str("path", r.URL.EscapedPath()).
				Dur("duration[ms]", time.Duration(time.Since(start).Milliseconds())).
				Interface("response-headers", wrapped.Header()).
				Msg("")
		})
	}
}

func getHeaders(r *http.Request, pexchange *c.Exchange) {
	for key, value := range r.Header {
		pexchange.Header[key] = value
	}
}

func (self *SourceHttp) Register(cctx c.CoreContext, pattern *string, endpoint *c.Endpoint, mux *mux.Router) {
	mux.Handle(*pattern, loggingMiddleware(cctx.Config.Logger)(self.handle(cctx, endpoint.To, endpoint.Params)))
}

/*
func GetBytes(val interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(val)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}*/

func (self *SourceHttp) handle(cctx c.CoreContext, f c.FlowFunc, p *c.Params) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//cctx := c.NewCoreContext(ctx)
		/*defer func() {
			log.Info("BEFORE CANCEL")
			cancel()
			log.Info("AFTER CANCEL")
		}()*/

		/*var body []byte
		if r.Body == http.NoBody {
		}*/

		if body, err := ioutil.ReadAll(r.Body); err == nil {
			defer r.Body.Close()

			pexchange := &c.Exchange{
				map[string]interface{}{c.ID: strconv.Itoa(1)},
				"string",
				body,
				p,
				nil,
			}

			getHeaders(r, pexchange)
			//!out := core.OrDone(
			//!	core.Raw(ctx, pexchange),
			//!)

			//log.Warnf("##### out is: %+v", *out)

			//!res := core.OrDone(f(out)) //TODO: Handle the result
			res := c.OrDone(cctx, f(cctx, c.Raw(cctx, pexchange))) //TODO: Handle the result
			//OK: out := core.Raw(ctx, pexchange)
			//OK: res := core.OrDone(f(out)) //TODO: Handle the result

			out, _ := <-res.Stream //the stream is closed if ok==false
			/*body, _ := GetBytes(out.Body)
			_, err := w.Write(body)
			*/
			fmt.Printf("Jestem %+v\n", out)
			if out.Error != nil { //!#
				fmt.Printf("Jestem1")
				http.Error(w, out.Error.Error(), http.StatusInternalServerError)
				return
			} else {

				fmt.Printf("Jestem2")
				w.WriteHeader(http.StatusOK)
			}

			fmt.Printf("Jestem3")
			switch v := out.Body.(type) {
			case string:
				io.WriteString(w, v)
			case []byte:
				w.Write(v)
			default:
				fmt.Fprintf(w, "%+v", v)
			}

		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

var (
	egressRequestDuration = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "golx_egress_request_duration_seconds",
		Help:    "Histogram for the runtime of a simple example function.",
		Buckets: prometheus.LinearBuckets(0.01, 0.01, 10),
	})
)

func init() {
	// Metrics have to be registered to be exposed:
	prometheus.MustRegister(egressRequestDuration)
}

func Http(cctx c.CoreContext, pin *c.Gate, fs ...func(cctx c.CoreContext, pexchange *c.Exchange) error) *c.Gate {

	fmt.Println("-------------------------- 2 PARAMS:", pin.Params)
	params := pin.Params //TODO: can be nil if Http is run by To indtead of ToP
	//return pin.To(core.Log)
	return pin.To(cctx, c.Transform, func(cctx c.CoreContext, pe *c.Exchange) error {

		// https://prometheus.io/docs/practices/histograms/ for differences.
		timer := prometheus.NewTimer(egressRequestDuration)
		defer timer.ObserveDuration()

		fmt.Printf("////HTTP START for params: %+v\n\n\n", params)
		method, ok := (*params)["method"].(string)
		if !ok {
			//pe.Error = HttpError{c.WrapError(nil, "method parameter not set")}
			return HttpError{c.WrapError(nil, "method parameter not set")}
		}
		url, ok := (*params)["url"].(string)
		if !ok {
			//pe.Error = HttpError{c.WrapError(nil, "url paramter not set")}
			return HttpError{c.WrapError(nil, "url parameter not set")}
		}
		fmt.Printf("////HTTP PARAMS: METHOD: %+v; URL: %v\n\n\n", method, url)
		netTransport := &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 5 * time.Second,
		}

		client := &http.Client{
			Timeout:   time.Second * 10, //TODO: timeout to Params
			Transport: netTransport,
		}

		var bodyToSend io.Reader
		switch v := pe.Body.(type) {
		case string:
			bodyToSend = bytes.NewBufferString(pe.Body.(string))
		case []byte:
			bodyToSend = bytes.NewBuffer(pe.Body.([]byte))
		/*case time.Time:
		bodyToSend = bytes.NewBufferString((pe.Body.(time.Time)).String())
		*/
		default:
			fmt.Printf("Type: %+v", v)
			panic("Unsupported body type to send in a http request")
		}

		req, err := http.NewRequestWithContext(cctx, method, url, bodyToSend)
		if err != nil {
			return HttpError{c.WrapError(err, "Can't create new request.")}
		}

		//{prometheus

		resp, err := client.Do(req)
		if err != nil {
			//pe.Error = HttpError{c.WrapError(err, "Can't perfom the request call.")}
			return HttpError{c.WrapError(err, "Can't perfom the request call.")}
		}

		fmt.Println("////HTTP /1/")
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			//pe.Error = HttpError{c.WrapError(err, "Can't read response body.")}
			return HttpError{c.WrapError(err, "Can't read response body.")}
		}
		fmt.Println("////HTTP /3/")

		fmt.Printf("///body %+v", string(body))

		pe.BodyType = "http-response"
		pe.Body = string(body)

		fmt.Printf("///pe.Body %+v", string(body))
		/*
			for _, f := range fs {
				f(pe)
			}*/

		fmt.Println("///HTTP END")
		return nil
	},
	)
}
