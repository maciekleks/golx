package core

import (
	"context"
	"os"
	"strings"
	"testing"

	"github.com/Jeffail/gabs/v2"
	"github.com/rs/zerolog"
)

func mapReduce(parallel bool) ([]string, *Gate) {
	ctx := context.TODO()
	logger := zerolog.New(os.Stdout).Level(zerolog.NoLevel)
	cctx := New(ctx, &ServeConfig{Logger: &logger})

	expected := []string{"H_E_L_L_O_", "W_O_R_L_D_"}

	ins := []*Exchange{
		&Exchange{
			map[string]interface{}{ID: 1},
			"string",
			"hello",
			nil,
			nil,
		},
		&Exchange{
			map[string]interface{}{ID: 2},
			"string",
			"world",
			nil,
			nil,
		},
	}

	return expected, Raw(cctx, ins...).
		Map(cctx, func(cctx CoreContext, in *Exchange, out chan<- *Exchange) error {
			cctx.Config.Logger.Info().Msg("STARTED.........................................................")
			defer close(out)
			for _, ch := range in.Body.(string) {
				//fmt.Println("\nch:", string(ch))
				out <- &Exchange{Header: in.Header, BodyType: "char", Body: strings.ToUpper(string(ch)), Params: nil}
			}
			return nil
		}).
		To(cctx, Log).
		To(cctx, Transform, func(cctx CoreContext, e *Exchange) error {

			cctx.Config.Logger.Debug().Msgf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! TRANSFORM: %+v", e)
			e.Body = e.Body.(string) + "_"
			return nil
		}).
		Reduce(cctx, func(cctx CoreContext, accumulator *Exchange, current *Exchange, index int) error {
			if index == 0 {
				accumulator.Body = ""
			}
			accumulator.Body = accumulator.Body.(string) + current.Body.(string)
			return nil
		}, parallel)
}

func TestMapReduceParralel(t *testing.T) {
	logger := zerolog.New(os.Stdout).Level(zerolog.NoLevel)
	cctx := New(context.TODO(), &ServeConfig{Logger: &logger})

	expected, route := mapReduce(true)

	contains := func(a []string, x string) bool {
		for _, n := range a {
			if x == n {
				return true
			}
		}
		return false
	}

	for exchange := range route.Stream {
		got := exchange.Body.(string)

		//due to parallelism the order of result are not predictable
		if !contains(expected, got) {
			t.Errorf("got = %s; want one of %v", got, expected)

		}
	}

	cctx.Wait()

}

func TestMapReduceSequential(t *testing.T) {
	logger := zerolog.New(os.Stdout).Level(zerolog.NoLevel)
	cctx := New(context.TODO(), &ServeConfig{Logger: &logger})

	expected, route := mapReduce(false)

	i := 0
	for exchange := range route.Stream {
		cctx.Config.Logger.Debug().Msgf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! FINAL: %+v", exchange)
		got := exchange.Body.(string)

		if got != expected[i] {
			t.Errorf("got = %s; want %s", got, expected[i])
		}
		i = i + 1
	}

	cctx.Wait()
}

// Simple test to show features of golx rather than an optimized way of dealing with JSON payloads
func TestMapReduceParralelJSON(t *testing.T) {
	logger := zerolog.New(os.Stdout).Level(zerolog.NoLevel)
	cctx := New(context.TODO(), &ServeConfig{Logger: &logger})

	const json = `
	{
		"name": {"first": "Tom", "last": "Anderson"},
		"age":37,
		"children": ["Sara","Alex","Jack"],
		"fav.movie": "Deer Hunter",
		"friends": [
			{"first": "Dale", "last": "Murphy", "age": 44, "nets": ["ig", "fb", "tw"]},
			{"first": "Roger", "last": "Craig", "age": 68, "nets": ["fb", "tw"]},
			{"first": "Jane", "last": "Murphy", "age": 47, "nets": ["ig", "tw"]}
		]
	}`

	const expected = `{"friends":["Dale Murphy","Roger Craig","Jane Murphy"],"name":"Tom Anderson"}`

	in := &Exchange{
		map[string]interface{}{ID: 1},
		"string",
		json,
		nil,
		nil,
	}

	route := Raw(cctx, in).
		//Transform exchange to gabs.Container
		To(cctx, Transform, func(cctx CoreContext, e *Exchange) error {
			jsonParsed, err := gabs.ParseJSON([]byte(json))
			if err != nil {
				return err
			}

			e.BodyType = "*gabs.Container"
			e.Body = jsonParsed

			return nil
		}).
		//Transform firstName and LastName of the top
		To(cctx, Transform, func(cctx CoreContext, e *Exchange) error {

			src := e.Body.(*gabs.Container)
			firstName, _ := src.Path("name.first").Data().(string)
			lastName, _ := src.Path("name.last").Data().(string)

			jsonObj := gabs.New()
			jsonObj.Set(firstName+" "+lastName, "name")
			jsonObj.Array("friends")
			for _, friend := range src.S("friends").Children() {
				jsonObj.ArrayAppend(friend.Data(), "friends")
			}
			e.Body = jsonObj
			return nil
		}).
		Map(cctx, func(cctx CoreContext, in *Exchange, out chan<- *Exchange) error {
			defer close(out)

			src := in.Body.(*gabs.Container)
			for _, friend := range src.S("friends").Children() {
				jsonObj := gabs.New()
				jsonObj.Set(src.Path("name"), "name")
				jsonObj.Array("friends")
				jsonObj.ArrayAppend(friend.Data(), "friends") //one element array for each next gate
				out <- &Exchange{Header: in.Header, BodyType: in.BodyType, Body: jsonObj}
			}
			return nil
		}).
		To(cctx, Log).
		To(cctx, Transform, func(cctx CoreContext, e *Exchange) error {

			src := e.Body.(*gabs.Container)
			friend, _ := src.JSONPointer("/friends/0")

			jsonObj := gabs.New()
			jsonObj.Set(src.S("name"), "name")
			jsonObj.Array("friends")
			jsonObj.ArrayAppend(friend.S("first").Data().(string)+" "+friend.S("last").Data().(string), "friends")

			e.Body = jsonObj
			return nil
		}).
		Reduce(cctx, func(cctx CoreContext, accumulator *Exchange, current *Exchange, index int) error {
			if index == 0 {
				//First exchange will be the template one
				accumulator.Body = current.Body
			} else {
				child := current.Body.(*gabs.Container)
				target := accumulator.Body.(*gabs.Container)
				childElement, _ := child.JSONPointer("/friends/0")
				target.ArrayAppend(childElement, "friends")
			}
			return nil
		}, true)

	for exchange := range route.Stream {
		src := exchange.Body.(*gabs.Container)
		got := src.String()
		if got != expected {
			t.Errorf("got = %s; want %s", got, expected)
		}
	}

	cctx.Wait()

}
