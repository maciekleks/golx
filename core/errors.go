package core

import (
	"fmt"
	//log "github.com/sirupsen/logrus"
	"runtime/debug"
)

func (err CoreError) Error() string {
	return err.Message
}

func WrapError(err error, messagef string, msgArgs ...interface{}) CoreError {
	return CoreError{
		Inner:      err,
		Message:    fmt.Sprintf(messagef, msgArgs...),
		StackTrace: string(debug.Stack()),
		Misc:       make(map[string]interface{}),
	}
}

func HandleError(cctx CoreContext, key int, err error, message string) {
	//log.SetPrefix(fmt.Sprintf("[logID: %v]: ", key))
	cctx.Config.Logger.Error().Msgf("logId: %d, ERROR: %#v", key, err)
	fmt.Printf("[%v] %v", key, message)
}
