package core

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"
	"net/http/pprof"
	"os"
	"os/signal"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
)

//TODO: what is it for?
func getHeaders(r *http.Request, pexchange *Exchange) {
	for key, value := range r.Header {
		pexchange.Header[key] = value
	}
}

// src: https://blog.gopheracademy.com/advent-2017/kubernetes-ready-service/
func healthz() http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	}
}

//src: https://blog.gopheracademy.com/advent-2017/kubernetes-ready-service/
func readyz(isReady *atomic.Value) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) {
		if isReady == nil || !isReady.Load().(bool) {
			http.Error(w, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}

func waitShutdown(ctx context.Context, logger *zerolog.Logger, cncl func(), srv *http.Server) error {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGTERM, // kill -SIGTERM XXXX
		syscall.SIGQUIT) // kill -SIGQUIT XXXX*/

	sig := <-sigs
	logger.Info().Msgf("os.Interrupt - shutting down... (sig: %v)\n", sig)

	// terminate after second signal before callback is done
	go func() {
		<-sigs
		logger.Fatal().Msg("os.Kill - terminating...\n")
	}()

	gracefulCtx, cancelShutdown := context.WithTimeout(ctx, 5*time.Second)
	defer cancelShutdown()

	if err := srv.Shutdown(gracefulCtx); err != nil {
		logger.Error().Err(err).Msgf("Shutdown error")
		return err
	}

	logger.Info().Msg("Gracefully stopped\n")

	cncl()

	return nil
}

func WithLogger(logger *zerolog.Logger) ServeConfigOption {
	return func(config *ServeConfig) {
		config.Logger = logger
	}
}

func WithDefaultLogger() ServeConfigOption {
	return func(config *ServeConfig) {
		logger := zerolog.New(os.Stderr).With().Timestamp().Logger()
		config.Logger = &logger
	}
}

func WithTLS(tlsConfig *tls.Config) ServeConfigOption {
	return func(config *ServeConfig) {
		config.TLSConfig = tlsConfig
	}
}

// Test helper function
func (e Endpoint) MuxRouteTest(req *http.Request) *http.Response {
	//endpoint := r[k]
	source := e.From.(ISource)
	ctx := req.Context()
	router := mux.NewRouter()
	logger := zerolog.New(os.Stdout).Level(zerolog.Disabled)
	cctx := New(ctx, &ServeConfig{Logger: &logger})
	source.Register(cctx, &req.URL.Path, &e, router)

	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	resp := rr.Result()

	return resp
}

// Test helper function
func (e Endpoint) NonMuxRouteTest() {
	//endpoint := r[k]
	source := e.From.(ISource)
	ctx := context.TODO()
	logger := zerolog.New(os.Stdout).Level(zerolog.Disabled)
	cctx := New(ctx, &ServeConfig{Logger: &logger})
	source.Register(cctx, nil, &e, nil)
}

func (r Routes) Serve(addr string, opts ...ServeConfigOption) {
	//mux := http.NewServeMux()
	router := mux.NewRouter()
	mainCtx, mainCancel := context.WithCancel(context.Background())

	var config *ServeConfig = &ServeConfig{}
	if len(opts) < 1 {
		fmt.Println("Expected at least one ServeConfigOption.")
		os.Exit(1)
	}
	for _, opt := range opts {
		opt(config)
	}

	defer func() {
		config.Logger.Debug().Msg("cancel()")
	}()

	cctx := New(mainCtx, config)
	for pattern, endpoint := range r {
		source := endpoint.From.(ISource)
		source.Register(cctx, &pattern, &endpoint, router)
	}

	//prometheus metrics
	router.Path("/metrics").Handler(promhttp.Handler())

	//pprof: src: https://stackoverflow.com/questions/30560859/cant-use-go-tool-pprof-with-an-existing-server
	router.HandleFunc("/debug/pprof/", http.HandlerFunc(pprof.Index))
	router.HandleFunc("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
	router.HandleFunc("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
	router.HandleFunc("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
	router.HandleFunc("/debug/pprof/trace", http.HandlerFunc(pprof.Trace))

	//{src: https://blog.gopheracademy.com/advent-2017/kubernetes-ready-service/
	isReady := &atomic.Value{}
	isReady.Store(false)
	go func() {
		time.Sleep(5 * time.Second)
		isReady.Store(true)
	}()

	router.Handle("/healthz", healthz())
	router.Handle("/readyz", readyz(isReady))
	//}

	srv := &http.Server{
		Addr:         addr,
		Handler:      router,
		TLSConfig:    config.TLSConfig,
		BaseContext:  func(_ net.Listener) context.Context { return mainCtx },
		ReadTimeout:  10 * time.Second, //goroutines leaks; src: https://stackoverflow.com/questions/42238695/goroutine-in-io-wait-state-for-long-time
		WriteTimeout: 10 * time.Second, //goroutines leaks; src: https://stackoverflow.com/questions/42238695/goroutine-in-io-wait-state-for-long-time
	}

	go func() {
		if err := srv.ListenAndServeTLS("", ""); err != nil && err != http.ErrServerClosed {
			mainCancel() //Fatal exists
			//log.Fatalf("HTTP server ListenAndServe: %+v\n", err)
			config.Logger.Fatal().Stack().Err(err).Msg("HTTP server ListenAndServe")
		} else {
			config.Logger.Fatal().Stack().Err(err).Msg("HTTP server listening...")
		}
	}()

	if err := waitShutdown(mainCtx, config.Logger, mainCancel, srv); err != nil {
		config.Logger.Fatal().Stack().Err(err).Msg("Waiting shutdown error")
		os.Exit(1)
	}
	os.Exit(0)

}
