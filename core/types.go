package core

import (
	"crypto/tls"

	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
)

const ID = "Go-Lx-ID"
const ParentID = "Go-Lx-ParentID"

//const BodyTypeEnd = "END"

type ISource interface {
	//Handle(context.Context, FlowFunc)
	Register(CoreContext, *string, *Endpoint, *mux.Router)
}

type FlowFunc func(CoreContext, *Gate) *Gate

type Params map[string]interface{}

// The only object flows through the all route in Routes
type Exchange struct {
	Header   Params      //Parameters used in http requests
	BodyType string      //TODO: change to type
	Body     interface{} //Exchange body
	Params   *Params     //Additional parameters not matching Headers
	Error    error       //!#
}

/*type GateContext struct {
	Ctx    context.Context
	Cancel func()
}*/

type Gate struct {
	//Ctx    context.Context
	Stream <-chan *Exchange
	Params *Params //short-term reference - accessible in the nearest To/ToP function only
	Error  error
}

type CoreError struct {
	Inner      error
	Message    string
	StackTrace string
	Misc       map[string]interface{}
}

type Gates []*Gate
type GateChannel <-chan *Gate

type Endpoint struct {
	//From http.HandlerFunc
	From   ISource
	To     FlowFunc
	Params *Params //Params to be set in the Route Exchange
}

type Routes map[string]Endpoint

type ServeConfig struct {
	Logger    *zerolog.Logger
	TLSConfig *tls.Config
}

//Serve params
type ServeConfigOption func(*ServeConfig)

//Gate params
type GateOption func(*Params)
