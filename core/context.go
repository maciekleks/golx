package core

import (
	"context"
	"sync"
	"time"
)

type CoreContext struct {
	ctx       context.Context
	exception chan error
	wg        *sync.WaitGroup
	Config    *ServeConfig
	counter   int
	name      string
}

func (c CoreContext) Deadline() (time.Time, bool)       { return c.ctx.Deadline() }
func (c CoreContext) Done() <-chan struct{}             { return c.ctx.Done() }
func (c CoreContext) Err() error                        { return c.ctx.Err() }
func (c CoreContext) Value(key interface{}) interface{} { return c.ctx.Value(key) }
func (c CoreContext) Exception() <-chan error {
	return c.exception
}

func (c CoreContext) Go(f func() error, descriptors ...string) {
	c.counter = c.counter + 1
	c.wg.Add(1)
	go func() {
		c.Config.Logger.Debug().Msgf("Go start[%s]: %v %d", c.name, descriptors, c.counter)
		defer c.Config.Logger.Debug().Msgf("Go ends[%s]: %v %d", c.name, descriptors, c.counter)
		defer c.wg.Done()
		defer func() { c.counter = c.counter - 1 }()
		if err := f(); err != nil {
			//fmt.Printf("c: %+v\n", c)
			c.Config.Logger.Error().Err(err).Msg("Go function error.")
			//c.Config.Logger.Info().Msgf("Go function error. %+v", c)
			// println(c.counter)
			// println(c.exception)
			//c.exception <- err
			for { //empty statement

			}

		}
	}()
}

func (c CoreContext) Wait() {

	c.Config.Logger.Debug().Msgf("..............................................Waiting %s %d...", c.name, c.counter)
	c.wg.Wait()
	close(c.exception)
	c.Config.Logger.Debug().Msgf("..............................................Waiting %s %d Done.", c.name, c.counter)
}

func New(ctx context.Context, config *ServeConfig, names ...string) CoreContext {
	config.Logger.Debug().Msgf(".................................................New context started.")

	name := "no-name"
	if len(names) > 0 {
		name = names[0]
	}
	return CoreContext{ctx, make(chan error), &sync.WaitGroup{}, config, 0, name}
}

func WithSignal(gctx CoreContext, names ...string) (CoreContext, func(err error)) {
	//sig := make(chan struct{})
	//sig := make(chan struct{})
	name := "no-name"
	if len(names) > 0 {
		name = names[0]
	}

	gctx.Config.Logger.Debug().Msgf(".................................................New context with signal started.")
	ctx, _ := context.WithCancel(gctx.ctx)
	//return CoreContext{ctx, gctx.sig}, func() { close(gctx.sig) }
	return CoreContext{ctx, gctx.exception, gctx.wg, gctx.Config, 0, name}, func(err error) { /*close(gctx.sig)*/
		gctx.Config.Logger.Debug().Msg("before errSig")
		select {
		case gctx.exception <- err:
			gctx.Config.Logger.Debug().Msg("errSig not closed. Added in.")
		default:
			gctx.Config.Logger.Debug().Msg("errSig closed already. Can't add")
		}

	}
}

// use the same WaitGroup as the gctx param
func WithCancel(gctx CoreContext, names ...string) (CoreContext, func()) {
	name := "no-name"
	if len(names) > 0 {
		name = names[0]
	}
	ctx, cancel := context.WithCancel(gctx.ctx)
	return CoreContext{ctx, gctx.exception, gctx.wg, gctx.Config, 0, name}, func() {
		cancel()
	}
}

// created a new WaitGroup
func WithGroupCancel(gctx CoreContext, names ...string) (CoreContext, func()) {
	name := "no-name"
	if len(names) > 0 {
		name = names[0]
	}

	ctx, cancel := context.WithCancel(gctx.ctx)
	return CoreContext{ctx, make(chan error), &sync.WaitGroup{}, gctx.Config, 0, name}, func() {
		cancel()
	}
}
