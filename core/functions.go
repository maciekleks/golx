package core

import (
	"sync"
	"time"
)

func raw(cctx CoreContext, values ...*Exchange) <-chan *Exchange {
	valueStream := make(chan *Exchange)
	cctx.Go(func() error {
		//log.Info(">>>>>>>> GOROUTINE raw started.")
		defer close(valueStream)
		//defer log.Println("raw GO ends.....")
		//defer log.Info(">>>>>>>> GOROUTINE raw end.")
		//for {
		for _, v := range values {
			//log.Println("#################Value to channeling:", v)
			select {
			case <-cctx.Done():
				//log.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!Done comes.")
				return nil
			case valueStream <- v:
				//log.Println("Values come: ", *v)
			}
		}
		return nil
	}, "raw")
	return valueStream
}

func orDone(cctx CoreContext, c <-chan *Exchange) <-chan *Exchange {
	valStream := make(chan *Exchange)
	cctx.Go(func() error {
		defer close(valStream)
		for {
			select {
			case <-cctx.Done():
				//log.Println("############# OrDone - First Done")
				return nil
			case v, ok := <-c:
				if ok == false { //false if channel is closed
					//log.Println("############## OrDone - Stream is empty and closed.")
					return nil
				}
				//log.Println("########### OrDone - Before select.")
				select {
				case valStream <- v:
					//log.Println("########## OrDone - Val")
				case <-cctx.Done():
					//log.Println("########## OrDone - Second Done")
					//! default: //not working for http target
				}
			}
		}
	}, "orDone")
	return valStream
}

func sink(cctx CoreContext, c <-chan *Exchange) {
	cctx.Go(func() error {
		//log.Info(">>>>>>>> GOROUTINE end started.")
		//defer log.Println(">>>>>>>> GOROUTINE end end.")
		for {
			select {
			case <-cctx.Done():
				//log.Println("############# end - First Done")
				return nil
			case _, ok := <-c:
				if ok == false { //false if channel is closed
					//log.Println("############## end - Stream is empty and closed.")
					return nil
				}
				//log.Println("########## end - Val:", v)
			}
		}
	}, "sink")
}

func transform(cctx CoreContext, c <-chan *Exchange, fs ...func(cctx CoreContext, pe *Exchange) error) <-chan *Exchange {
	valStream := make(chan *Exchange)
	cctx.Go(func() error {
		defer close(valStream)
		for {
			select {
			case <-cctx.Done():
				//log.Println("transform - DONE")
				return nil
			case v, ok := <-c:
				if ok == false { //false if channel is closed
					//log.Printf("transform - Stream is empty and closed. v:%v, ok:%v", v, ok)
					return nil
				}
				for _, f := range fs {
					//if v.BodyType != BodyTypeEnd {
					//do not transform streering Exchanges
					if err := f(cctx, v); err != nil {
						v.Error = err //!#
						//return nil
						//!#return err //TODO: poważna rzecz bo w http nie będzie out (tj będzie nil)
						//cctx.Cancel()
					}
					//}
				}
				select {
				case valStream <- v:
					/*for _, f := range fs {
						f(v)
					}*/
					//log.Println("transform - Result ready to consume.")
				case <-cctx.Done():
					//log.Println("transform - DONE 2")
				}
			}
		}
	}, "transform")
	return valStream
}

func tee(cctx CoreContext, in <-chan *Exchange) (_, _ <-chan *Exchange) {
	out1 := make(chan *Exchange)
	out2 := make(chan *Exchange)
	cctx.Go(func() error {
		//log.Info(">>>>>>>> GOROUTINE tee started.")
		defer close(out1)
		defer close(out2)
		//defer log.Println(">>>>>>>> GOROUTINE tee end.")
		for val := range orDone(cctx, in) {
			var out1, out2 = out1, out2
			for i := 0; i < 2; i++ { //non blocking sending to a channel
				select {
				case <-cctx.Done():
				case out1 <- val:
					//log.Println(">>>>> TEE: out1")
					out1 = nil
				case out2 <- val:
					//log.Println(">>>>> TEE: out2")
					out2 = nil
				}
			}
		}
		return nil
	}, "tee")
	return out1, out2
}

func join(cctx CoreContext, channels ...<-chan *Exchange) <-chan *Exchange {
	var wg sync.WaitGroup
	multiplexedStream := make(chan *Exchange)

	multiplex := func(c <-chan *Exchange) {
		//log.Info(">>>>>>>> GOROUTINE join started.")
		defer wg.Done()
		//defer log.Println(">>>>>>>> GOROUTINE join end.")
		for i := range c {
			//log.Printf(">>>>>> multiplex i:%+v", i)
			select {
			case <-cctx.Done():
				return
			case multiplexedStream <- i:
			}
		}
	}

	// Select from all the channels
	wg.Add(len(channels))
	for _, c := range channels {
		go multiplex(c)
	}

	// Wait for all the reads to complete
	go func() {
		//log.Info(">>>>>>>> GOROUTINE join.wait started.")
		//defer log.Println(">>>>>>>> GOROUTINE join.wait end.")
		wg.Wait()
		close(multiplexedStream)
	}()
	return multiplexedStream
}

func fanMap(ins []*Gate, f func(i *Gate) <-chan *Exchange) []<-chan *Exchange {
	out := make([]<-chan *Exchange, len(ins))
	for i, v := range ins {
		out[i] = f(v)
	}
	return out
}

func Raw(cctx CoreContext, values ...*Exchange) *Gate {
	out := raw(cctx, values...)
	return &Gate{Stream: out}
}

func Timer(cctx CoreContext, d time.Duration, f func(int64, time.Time) *Exchange) *Gate {
	valueStream := make(chan *Exchange)
	pin := &Gate{Stream: valueStream}
	cctx.Config.Logger.Debug().Msg("Timer:: Started")
	tctx, cancel := WithCancel(cctx)
	cctx.Go(func() error {
		//log.Println(">>>>>>>> GOROUTINE Timer started.")
		defer close(valueStream)
		//defer log.Println(">>>>>>>> GOROUTINE Timer end.")

		var iter int64 = 0
		//pulse := time.Tick(d)
		pulse := time.NewTicker(d)
		defer pulse.Stop()

		cctx.Config.Logger.Debug().Msgf("Timer:: Duration: %v", d)
		for {
			select {
			case <-tctx.Done():
				cctx.Config.Logger.Debug().Msgf("Timer:: Gets cctx.Done() due to %s", cctx.Err())
				return nil
			case tick := <-pulse.C:
				cctx.Config.Logger.Debug().Msg("Timer:: Tick comes in.")
				iter++
				valueStream <- f(iter, tick)
			case exception := <-tctx.Exception():
				cctx.Config.Logger.Error().Err(exception).Msg("Timer exception:")
				cancel()

			}
		}
		return nil
	}, "Timer")
	return pin
}

//
func OrDone(cctx CoreContext, g *Gate, f ...func(pexchange *Exchange) error) *Gate {
	out := orDone(cctx, g.Stream)
	return &Gate{Stream: out}
}

//func Sink(cctx CoreContext, g *Gate, f ...func(pexchange *Exchange) error) {
//	sink(cctx, g.Stream)
//}

//Puts everyting into sink
//Returns nil what means that it could not be pipe any longer
func Sink(cctx CoreContext, g *Gate, f ...func(cctx CoreContext, pexchange *Exchange) error) *Gate {
	sink(cctx, g.Stream)
	return nil
}

// ...
func Log(cctx CoreContext, ins *Gate, f ...func(cctx CoreContext, pexchange *Exchange) error) *Gate {
	out := transform(cctx, ins.Stream, func(cctx CoreContext, pexchange *Exchange) error {
		cctx.Config.Logger.Info().Msgf("Exchange: %v", *pexchange)
		return nil
	})

	return &Gate{Stream: out}
}

func Transform(cctx CoreContext, ins *Gate, f ...func(cctx CoreContext, pexchange *Exchange) error) *Gate {
	out := transform(cctx, ins.Stream, f...)

	return &Gate{Stream: out}
}

func Tee(cctx CoreContext, in *Gate) []*Gate {
	out1, out2 := tee(cctx, in.Stream)
	arr := make([]*Gate, 2)

	arr[0] = &Gate{Stream: out1}
	arr[1] = &Gate{Stream: out2}

	return arr
}

func FanIn(cctx CoreContext, ins []*Gate) *Gate {
	//TODO: kazdy in może mieć swój kontekst
	out := join(cctx, fanMap(ins, func(i *Gate) <-chan *Exchange { return i.Stream })...)

	return &Gate{Stream: out}
}

func (selfs Gates) Join(cctx CoreContext, coref func(cctx CoreContext, ins []*Gate) *Gate) *Gate {
	return coref(cctx, selfs)
}

func (self *Gate) Fork(cctx CoreContext, coref func(cctx CoreContext, ins *Gate) []*Gate, iopf func(cctx CoreContext, ins *Gate) *Gate) Gates {
	ret := coref(cctx, self)
	outs := make([]*Gate, len(ret))
	for indx, i := range ret {
		outs[indx] = iopf(cctx, i)
	}
	return outs
}

func (self *Gate) Map(cctx CoreContext, splitter func(CoreContext, *Exchange, chan<- *Exchange) error) GateChannel {
	gateStream := make(chan *Gate)

	cctx.Go(func() error {
		defer close(gateStream)
		defer cctx.Config.Logger.Debug().Msg("Split::!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Go ends....")

		for {
			select {
			case <-cctx.Done():
				cctx.Config.Logger.Debug().Msg("Split:: Done 1.0")
				return nil
			case exchange, ok := <-self.Stream:
				cctx.Config.Logger.Debug().Msg("Split:: $$$$$$$$$$$$$$$$$$$$ Exchange comes in")
				if !ok {
					cctx.Config.Logger.Debug().Msg("Split:: self.Stream empty")
					return nil
				}

				cctx.Config.Logger.Debug().Msg("Split:: self.Stream exchange comes")

				scratchStream := make(chan *Exchange)
				cctx.Go(func() error {
					err := splitter(cctx, exchange, scratchStream)
					if err != nil {
						panic("//////////////////")
					}
					return nil
				}, "scratchStream")
				gateStream <- &Gate{Stream: scratchStream}

			}
		}
	}, "splitter")
	return gateStream
}

func (selfs GateChannel) Reduce(cctx CoreContext, reduce func(cctx CoreContext, accumulator *Exchange, currentValue *Exchange, index int) error, parallel bool) *Gate {
	out := make(chan *Exchange)

	innerCctx, _ := WithGroupCancel(cctx)

	cctx.Go(func() error {

		innerCctx.Config.Logger.Debug().Msg("Inner cctx wait started....")
		innerCctx.Wait()
		close(out)

		innerCctx.Config.Logger.Debug().Msg("Inner cctx wait doner....")
		return nil
	}, "innerCcxt wait")

	reducer := func(gate *Gate, out chan<- *Exchange) func() error {
		return func() error {
			innerCctx.Config.Logger.Info().Msgf("Aggregate::!Gate comes in")
			accumulator := &Exchange{} //TODO: copy incomming payload
			i := 0
			for {
				select {
				case <-innerCctx.Done():
					return nil
				case scratchExchange, ok := <-gate.Stream:
					innerCctx.Config.Logger.Info().Msgf("Aggregate:: Exchange comes in %v", scratchExchange)
					if !ok {
						//out <- &Exchange{ /*Header: scratchExchange.Header, BodyType: scratchExchange.BodyType,*/ Body: str}
						out <- accumulator
						innerCctx.Config.Logger.Info().Msgf("Aggregate:: Exchange created!!!!!!!!!!!!!!!!!!!!!!!!!")
						return nil
					}
					reduce(innerCctx, accumulator, scratchExchange, i)
					i++
				} //select
			} //for
		}
	}

	innerCctx.Go(func() error {
		for {
			select {
			case <-innerCctx.Done():
				innerCctx.Config.Logger.Debug().Msg("Aggregate:: Done 1")
				return nil
			case gate, ok := <-selfs:
				if !ok {
					innerCctx.Config.Logger.Debug().Msg("Aggregate:: Empty")
					return nil
				}
				if parallel {
					innerCctx.Go(reducer(gate, out), "reduce")
				} else {
					reducer(gate, out)()
				}
			}

		}
	}, "Aggregate")

	cctx.Config.Logger.Info().Msg("Aggregate:: ends gracefully")
	return &Gate{Stream: out}

}

func (selfs GateChannel) To(cctx CoreContext, coref func(cctx CoreContext, i *Gate, os ...func(cctx CoreContext, pe *Exchange) error) *Gate, fs ...func(cctx CoreContext, pe *Exchange) error) GateChannel {
	gateStream := make(chan *Gate)
	cctx.Go(func() error {
		defer close(gateStream)
		for {
			select {
			case <-cctx.Done():
				cctx.Config.Logger.Debug().Msg("To on GateChannel:: ends gracefully")
				return nil
			case gate, ok := <-selfs:
				if !ok {
					cctx.Config.Logger.Debug().Msg("To on GateChannel:: stream ends.")
					return nil
				}
				gateStream <- coref(cctx, gate, fs...)

			} //select
		}

	}, "To on GateChannel")

	return gateStream

}

func (self *Gate) To(cctx CoreContext, coref func(cctx CoreContext, i *Gate, os ...func(cctx CoreContext, pe *Exchange) error) *Gate, fs ...func(cctx CoreContext, pe *Exchange) error) *Gate {
	return coref(cctx, self, fs...)
}

//Applyies options into Gate
func ApplyOptions(opts ...GateOption) *Params {
	var params = &Params{}

	for _, opt := range opts {
		opt(params)
	}

	return params
}

//Routes traffic into coref applying GateOptions into the current gate to be taken by coref
func (self *Gate) ToP(cctx CoreContext, coref func(cctx CoreContext, i *Gate, os ...func(cctx CoreContext, pe *Exchange) error) *Gate, params *Params, fs ...func(cctx CoreContext, pe *Exchange) error) *Gate {
	self.Params = params
	cctx.Config.Logger.Debug().Msgf("ToP params: %v", self.Params)
	defer func() {
		//log.Println("PARAMS---01:", self.Params)
		self.Params = nil
	}()
	return coref(cctx, self, fs...)
}
