Table of content

- [What is golx](#what)
- [Why golx](#why-golx)
- [Features](#features)
- [Updating](#updating)
- [Compiling](#compiling)
- [Testing](#testing)

# What is golx
Golang library mimics Apache Cammel driven by Golang concurrency patterns.

# Why golx
Just for fun.

# Features
See examples: [golx-examples](https://gitlab.com/maciekleks/golx-examples)

# Updating
```
go get -u ./components/**
go mod tidy
```

# Compiling
```
go build ./components/**
```

# Testing
```
go test ./components/**
```
